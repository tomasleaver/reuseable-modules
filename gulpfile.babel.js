'use strict';

import gulp from 'gulp';

// Utilities
import fs from 'fs';
import gutil from 'gulp-util';
import argv from 'yargs';
import runSequence from 'run-sequence';
import concat from 'gulp-concat';
import eventStream from 'event-stream';
import browserSync from 'browser-sync';
import preprocess from 'gulp-preprocess';

// Styles
import sass from 'gulp-sass';
import cssmin from 'cssmin';
import sassLint from 'gulp-sass-lint';
import autoprefixer from 'gulp-autoprefixer';

// Scripts
import eslint from 'gulp-eslint';
import uglify from 'gulp-uglify';
import sourcemaps from 'gulp-sourcemaps';
import babel from 'gulp-babel';

// End tasks
const pkg = JSON.parse(fs.readFileSync('package.json'));
const isProduction = argv.production !== void 0;
const noop = () => {};
const proxyDomain = 'http://reuseable-modules.local/'

// Styling tasks

gulp.task('sass:lint', () => {
    return gulp.src([
            `${pkg.dir.styles.dev}**/*.s+(a|c)ss`
    ])
        .pipe(sassLint({
            files: {
                ignore: [
                    '**/vendor/**/*.scss'
                ]
            },
            rules: {
                // Severity 0 (disabled)
                // Severity 1 (warning)
                // Severity 2 (error)
                'clean-import-paths': 0, // need to fix this showing warnings and it needs to be changed
                'no-ids': 1,
                'bem-depth': 1,
                'no-misspelled-properties': 1,
                'space-after-colon': true,
                'nesting-depth': 4,
                'no-css-comments': 0,
                'no-vendor-prefixes': 0,
                'mixins-before-declarations': false,
                'leading-zero': 0,
                'border-zero': 0,
                'property-sort-order': 0,
                'class-name-format': [1,
                    {
                        'allow-leading-underscore': true,
                        'convention': 'hyphenatedbem'
                    }
                ],
                'variable-name-format': [1,
                    {
                        'allow-leading-underscore': false,
                        'convention': 'hyphenatedlowercase'
                    }
                ],
                'indentation': [1,
                    {
                        'size': 4
                    }
                ],
                'no-url-domains': 0,
                'no-url-protocols': 0,
                'no-color-literals': false
            }
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError());
});

gulp.task('sass:compile', () => {

    return gulp.src(`${pkg.dir.styles.dev}main.scss`)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'safari 5', 'ie 10', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: false
        }))
        .pipe(concat({
            path: 'main.css'
        }))
        .pipe(isProduction ? cssmin() : gutil.noop())
        .pipe(gulp.dest(pkg.dir.styles.prod))
        .pipe(browserSync.stream());
});

// Scripting tasks

gulp.task('js:lint', () => {
    // ESLint ignores files with 'node_modules' paths.
    // So, it's best to have gulp ignore the directory as well.
    return gulp.src([
            `!${pkg.dir.scripts.dev}vendors/**/*.js`,
            `${pkg.dir.scripts.dev}**/*.js`
    ])
        // eslint() attaches the lint output to the 'eslint' property
        // of the file object so it can be used by other modules.
        // see .eslintrc.json file for implemented config
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

gulp.task('js:compile', () => {

    return eventStream.merge(
            gulp.src([
                `${pkg.dir.scripts.dev}vendors/**/*.js`,
                `${pkg.dir.scripts.dev}helpers/**/*.js`,
                `${pkg.dir.scripts.dev}modules/**/*.js`,
                `${pkg.dir.scripts.dev}index.js`
            ])
            .pipe(babel())
            .pipe(isProduction ? gutil.noop() : sourcemaps.init())
            .pipe(concat({
                path: 'main.js'
            }))
            .pipe(isProduction ? preprocess() : gutil.noop())
            .pipe(isProduction ? uglify().on('error', (e) => { console.log(e); }) : gutil.noop())
            .pipe(isProduction ? gutil.noop() : sourcemaps.write('./'))
            .pipe(gulp.dest(pkg.dir.scripts.prod))
        );
});

// Fonts task

gulp.task('fonts', () => {

    return gulp.src([
            `${pkg.dir.fonts.dev}**/*`
    ])
        .pipe(gulp.dest(pkg.dir.fonts.prod));

});

// Compiling tasks

gulp.task('copy:base', () => {
    return gulp.src([
            `${pkg.dir.prod}**/*`
    ], {
        base: './'
    })
        .pipe(gulp.dest(pkg.dir.webroot))
        .on('end', () => {
            gutil.log(`Copied to: ${pkg.dir.webroot}`);
        });
});

gulp.task('serve', ['sass:compile', 'js:compile'], () => {
    browserSync.init({
        proxy: proxyDomain
    });

    gulp.watch([`${pkg.dir.styles.dev}**/*.scss`], () => {
        runSequence('sass:lint', 'sass:compile', 'copy:base', browserSync.reload);
    });

    gulp.watch([`${pkg.dir.scripts.dev}**/*.js`], () => {
        runSequence('js:lint', 'js:compile', 'copy:base', browserSync.reload);
    });
});

gulp.task('watch', ['serve'], () => {

    gulp.watch([`${pkg.dir.fonts.dev}**/*`], () => {
        runSequence('fonts', ['copy:base']);
    });

});

gulp.task('build', () => {
    runSequence('fonts', 'sass:lint', 'sass:compile', 'js:lint', 'js:compile', 'copy:base');
});

gulp.task('teamCity', () => {
    runSequence('fonts', 'sass:compile', 'js:compile');
});