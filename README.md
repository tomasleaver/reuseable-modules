=================================================================
A basic repo containing some re-useable modules.
=================================================================


Modules:
--------------------
Accordions, using CSS.
Accordions, using JS.

Tabordions, using both CSS & JS.

A JS module to update the background image of a div container, using data-attributes. This can easily be amended to update img src instead.



Utility modules:
--------------------

Device-check, as you would expect.

Resetable-timeout.
Pass this a timeout ID on window.resize and it will reset the original timeout that was called. This is so that multiple timeouts aren't created, rather each time it is reset, so that overall the timeout will only run once the window has finished resizing. Example usage within other modules.



Views:
--------------------
Included to provide example usage and the expected markup.



=================================================================
Setup
=================================================================


Set this up as a site in IIS, targetting "C:\inetpub\wwwroot\reuseable-modules\Website".
Add "127.0.0.1	reuseable-modules.local" to your hosts file.
Pull down the repo to your local machine, and run "npm install".
You will need to manually copy over the index.html and "...\reuseable-modules\Views" folder to the "C:\inetpub\wwwroot\reuseable-modules\Website" folder manually.
Then run "gulp build"; this will copy over the FE assets.
The site will be viewable then at "reuseable-modules.local/" or by running "gulp watch" a local instance will be spun up which will refresh on code updates.



=================================================================
Origin
=================================================================


Basic structure, gulp file, fonts, placeholders etc were all taken from Nelsons, where these modules were originally written.
