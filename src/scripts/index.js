jQuery.noConflict();

let UTILS = UTILS || {};
let APP = APP || {};

APP.app = (() => {
    'use strict';

    const init = () => {
        UTILS.includeHTML.init();

        // Timeout is to ensure HTML partials are first loaded by JS
        setTimeout(() => {
            APP.updateBackgroundImages.init();
            APP.accordion.init();
            APP.tabordion.init();
        }, 1000);
    };

    return {
        init: init
    };
})();

jQuery(document).ready(() => {
    'use strict';

    APP.app.init();
});
