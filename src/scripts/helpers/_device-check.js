let UTILS = UTILS || {};

UTILS.deviceCheck = (() => {
    'use strict';

    const _tabletWidth = '768';
    const _smallDesktopWidth = '1024';
    const _desktopWidth = '1280';
    const _mediumDesktopWidth = '1440';

    const isMobile = () => {
        return window.innerWidth < _tabletWidth;
    };

    const isTablet = () => {
        return window.innerWidth <= _smallDesktopWidth;
    };

    const isSmallDesktop = () => {
        return window.innerWidth < _desktopWidth;
    };

    const isDesktop = () => {
        return window.innerWidth < _mediumDesktopWidth;
    };

    const isLargeDesktop = () => {
        return _mediumDesktopWidth <= window.innerWidth;
    };

    const getDeviceSize = () => {
        if (isMobile()) {
            return 'mobile';
        } else if (isTablet()) {
            return 'tablet';
        } else if (isSmallDesktop()) {
            return 'desktop';
        } else if (isDesktop()) {
            return 'medium-desktop';
        } else if (isLargeDesktop()) {
            return 'large-desktop';
        }
    };

    return {
        isMobile: isMobile,
        isTablet: isTablet,
        isSmallDesktop: isSmallDesktop,
        isDesktop: isDesktop,
        isLargeDesktop: isLargeDesktop,
        getDeviceSize: getDeviceSize
    };
})();
