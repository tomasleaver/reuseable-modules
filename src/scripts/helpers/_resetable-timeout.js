let UTILS = UTILS || {};

/**
 * resetableTimeout
 *
 * @description This function is to initialise a re-setable timeout to execute passed in function at the end of the delay, if not re-called during timeout.
 * @param {Number} timeoutID The id to track the timeout against.
 * @param {Function} cb The function to call at the end of the timeout duration.
 * @param {Number} delay The delay in ms for the timeout to wait.
 *
 * Example usage: timeoutID = UTILS.resetableTimeout(timeoutID, callbackFunctionName, 250);
 * See _update-background-images.js for another example
 */
UTILS.resetableTimeout = (() => {
    'use strict';

    return (timeoutID, cb, delay) => {
        if (typeof timeoutID === 'undefined') {
            timeoutID = window.setTimeout(cb, delay);
        } else {
            window.clearTimeout(timeoutID);
            timeoutID = window.setTimeout(cb, delay);
        }
        return timeoutID;
    };
})();
