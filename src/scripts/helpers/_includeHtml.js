let UTILS = UTILS || {};

UTILS.includeHTML = (() => {
    'use strict';

    const _includeHTML = () => {
        let htmlElements;
        /*loop through a collection of all HTML elements:*/
        htmlElements = document.getElementsByTagName('*');
        for (let i = 0, len = htmlElements.length; i < len; i++) {
            let elmnt = htmlElements[i];
            /*search for elements with a certain atrribute:*/
            let file = elmnt.getAttribute('include-html');
            if (file) {
                /*make an HTTP request using the attribute value as the file name:*/
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        if (this.status == 200) { elmnt.innerHTML = this.responseText; }
                        if (this.status == 404) { elmnt.innerHTML = 'Page not found.'; }
                        /*remove the attribute, and call this function once more:*/
                        elmnt.removeAttribute('include-html');
                        _includeHTML();
                    }
                };
                xhttp.open('GET', file, true);
                xhttp.send();
                return;
            }
        }
    };

    const init = () => {
        _includeHTML();
    };

    return {
        init: init
    };
})();