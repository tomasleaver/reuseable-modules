let APP = APP || {};

APP.tabordion = (($, undefined) => {
    'use strict';

    const selectors = {
        module: 'tabordion',
        container: 'tabordion-container',
        content: 'tabordion__content',
        link: 'tabordion__tab',
    };

    const state = {
        openTabordions: []
    };

    const classes = {
        jsFallback: 'no-js-fallback',
        isActive: 'tabordion--is-active',
        transform: 'tabordion__content--transform'
    };

    let tabordions = [];
    let container;
    let originalHeight;
    let timeoutID;
    let deviceSize;

    const _initState = () => {
        // If sessionStorage exists, retrieve openTabordions
        if (sessionStorage.getItem('openTabordions')) {
            state.openTabordions = JSON.parse(sessionStorage.getItem('openTabordions'));
        }
    };

    const _initTabordions = () => {
        // Set IDs against each tabordion, setup clickEvents and open Tabordion if in sessionStorage
        for (let i = 0, len = tabordions.length; i < len; i++) {
            const id = `tabordions-${i}`;

            tabordions[i].id = id;
            tabordions[i].firstElementChild.addEventListener('click', () => { _handleClick(tabordions[i]); });

            if (state.openTabordions.indexOf(id) !== -1) {
                _openTabordion(tabordions[i], true);
            }

            // Add the transform property after initial load, so that DOM does not animate on load of the page.
            setTimeout(() => {
                tabordions[i].lastElementChild.classList.add(classes.transform);
            }, 600);
        }
    };

    const _handleClick = (tabordion) => {
        const index = state.openTabordions.indexOf(tabordion.id);

        if (index === -1) {
            let delay = 0;
            // If a tabordion is open, close that tabordion
            if (state.openTabordions.length > 0) {
                const previousTabordion = document.getElementById(state.openTabordions[0]);
                _closeTabordion(previousTabordion);
                delay = 500;
            }
            // Delay to allow for closing animation
            setTimeout(() => {
                _openTabordion(tabordion);
            }, delay);
        } else {
            // Only close on mobile, for accordion functionality
            if (UTILS.deviceCheck.isMobile()) {
                _closeTabordion(tabordion, index);
            }
        }
    };

    const _openTabordion = (tabordion, initFromSession) => {
        // Only record openTabordions in sessionStorage if this is not called from init
        if (!initFromSession) {
            state.openTabordions.push(tabordion.id);
            sessionStorage.setItem('openTabordions', JSON.stringify(state.openTabordions));
        }

        tabordion.classList.add(classes.isActive);
        _setHeight(tabordion);
    };

    const _closeTabordion = (tabordion, index) => {
        // If index is passed in, remove that id from state else reset
        if (typeof index !== 'undefined') {
            state.openTabordions.splice(index, 1);
        } else {
            state.openTabordions = [];
        }

        sessionStorage.setItem('openTabordions', JSON.stringify(state.openTabordions));

        tabordion.classList.remove(classes.isActive);
    };

    const _setHeight = (tabordion) => {
        if (!UTILS.deviceCheck.isMobile()) {
            setTimeout(() => {
                const height = tabordion.lastElementChild.getBoundingClientRect().height;
                if (typeof originalHeight === 'undefined') {
                    originalHeight = container.offsetHeight;
                }
                container.style.height = `${height + originalHeight}px`;
            }, 500);
        }
    };

    const _handleResizeDelay = () => {
        timeoutID = UTILS.resetableTimeout(timeoutID, _handleResize, 250);
    };

    const _handleResize = () => {
        _openDefault();

        const newSize = UTILS.deviceCheck.getDeviceSize();
        if (deviceSize !== newSize && newSize === 'mobile') {
            container.style.height = 'auto';
        }

    };

    const _openDefault = () => {
        if (!UTILS.deviceCheck.isMobile() && state.openTabordions.length === 0) {
            _openTabordion(tabordions[0]);
        }
    };

    /**
     * init
     *
     * @description Initializes the component by removing the no-js class from
     *   the component, setting up tabordion and attaching event listeners.
     */
    const init = () => {
        tabordions = document.getElementsByClassName(selectors.module);
        if (tabordions.length === 0) {
            return;
        }

        container = document.getElementsByClassName(selectors.container)[0];
        container.classList.remove(classes.jsFallback);

        _initState();
        _initTabordions();
        _openDefault();
        $(window).resize(_handleResizeDelay);
    };

    return {
        init: init
    };
})(jQuery);
