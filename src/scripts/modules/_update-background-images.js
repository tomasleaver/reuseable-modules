let APP = APP || {};

APP.updateBackgroundImages = (($, undefined) => {
    'use strict';

    const selectors = {
        module: '.js-update-background-image'
    };

    let timeoutID;
    let backgroundImages;
    let devicePath;

    const _updateBackgroundImages = () => {
        const imgPathData = _getCorrectDataAttribute();

        if (devicePath !== imgPathData) {
            backgroundImages.each(function () {
                const imgPath = $(this).data(imgPathData);
                const cssProperty = `url(${imgPath})`;

                $(this).css('background-image', cssProperty);
            });
            devicePath = imgPathData;
        }
    };

    const _getCorrectDataAttribute = () => {
        switch (UTILS.deviceCheck.getDeviceSize()) {
            case 'mobile':
                return 'mobile-path';
                break;
            case 'tablet':
            case 'desktop':
                return 'tablet-path';
                break;
            default:
                return 'desktop-path';
                break;
        }
    };

    const _handleResizeDelay = () => {
        timeoutID = UTILS.resetableTimeout(timeoutID, _updateBackgroundImages, 750);
    };

    const init = () => {
        backgroundImages = $(selectors.module);
        if (backgroundImages.length === 0) {
            return;
        }

        _updateBackgroundImages();
        $(window).resize(_handleResizeDelay);
    };

    return {
        init: init
    };
})(jQuery);
