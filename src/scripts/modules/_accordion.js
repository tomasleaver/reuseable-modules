let APP = APP || {};

APP.accordion = (() => {
    'use strict';

    const selectors = {
        module: 'accordion-js',
        multiple: 'accordion-js-container--multiple'
    };

    const state = {
        singleOpen: true,
        openAccordions: []
        // accordionIDs: [] // This may be used later, if module is amended to allow multiple accordion groups on a single page
    };

    const classes = {
        open: 'accordion-js--is-open'
    };

    let accordions = [];

    const _initState = () => {
        if (document.getElementsByClassName(selectors.multiple).length > 0) {
            state.singleOpen = false;
        }

        // If sessionStorage exists, retrieve openAccordions
        if (sessionStorage.getItem('openAccordions')) {
            state.openAccordions = JSON.parse(sessionStorage.getItem('openAccordions'));
        }
    };

    const _initAccordions = () => {
        // Set IDs against each accordion-js
        for (let i = 0, len = accordions.length; i < len; i++) {
            const id = `accordion-${i}`;
            // state.accordionIDs.push(id); // This may be used later, if module is amended to allow multiple accordion groups on a single page

            accordions[i].id = id;
            accordions[i].firstElementChild.addEventListener('click', () => { _handleClick(accordions[i]); });

            if (state.openAccordions.indexOf(id) !== -1) {
                _openAccordion(accordions[i], true);
            }
        }
    };

    const _handleClick = (accordion) => {
        const index = state.openAccordions.indexOf(accordion.id);

        if (index === -1) {
            // If not found, check if singleOpen is true
            if (state.singleOpen) {
                // If an accordion is open, close that accordion
                if (state.openAccordions.length > 0) {
                    const previousAccordion = document.getElementById(state.openAccordions[0]);
                    _closeAccordion(previousAccordion);
                }

                _openAccordion(accordion);
            } else {
                _openAccordion(accordion);
            }
        } else {
            _closeAccordion(accordion, index);
        }
    };

    const _openAccordion = (accordion, initFromSession) => {
        // Only record openAccordion in sessionStorage if this is not called from init
        if (!initFromSession) {
            state.openAccordions.push(accordion.id);
            sessionStorage.setItem('openAccordions', JSON.stringify(state.openAccordions));
        }

        accordion.classList.add(classes.open);
    };

    const _closeAccordion = (accordion, index) => {
        // If index is passed in, remove that id from state else reset
        if (typeof index !== 'undefined') {
            state.openAccordions.splice(index, 1);
        } else {
            state.openAccordions = [];
        }

        sessionStorage.setItem('openAccordions', JSON.stringify(state.openAccordions));

        accordion.classList.remove(classes.open);
    };

    const init = () => {
        accordions = document.getElementsByClassName(selectors.module);
        if (accordions.length === 0) {
            return;
        }

        _initState();
        _initAccordions();
    };

    return {
        init: init
    };
})();
